#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASH_TABLE_SIZE 100
#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 9 // Increased length to accommodate newline and null terminator
#define PROGRAM_LENGTH 5

// Structure to represent a student
typedef struct
{
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_LENGTH];
    char registrationNumber[REGISTRATION_LENGTH];
    char programCode[PROGRAM_LENGTH];
    float annualTuition;
} Student;

// Structure to represent a node in the linked list
typedef struct Node
{
    Student data;
    struct Node *next;
} Node;

// Structure to represent the hash table
typedef struct
{
    Node *head;
} HashTable;

// Function prototypes
void initHashTable(HashTable *hashTable);
int hash(char *registrationNumber);
void insertStudent(HashTable *hashTable, Student student);
void displayStudents(HashTable *hashTable);
void deleteStudent(HashTable *hashTable, char *registrationNumber);
void searchStudent(HashTable *hashTable, char *registrationNumber);
void sortStudents(HashTable *hashTable);
void exportRecords(HashTable *hashTable);
void swapStudents(Student *a, Student *b);

// Function to create a new student
Student createStudent()
{
    Student student;
    // Input validation loop
    while (1)
    {
        printf("Enter student name (up to 50 characters): ");
        fgets(student.name, MAX_NAME_LENGTH, stdin);
        // Check if the name is not empty
        if (strlen(student.name) == 1 && student.name[0] == '\n')
        {
            printf("Name cannot be empty.\n");
            continue;
        }
        // Remove the newline character if present
        if (student.name[strlen(student.name) - 1] == '\n')
        {
            student.name[strlen(student.name) - 1] = '\0';
        }
        // Check if the name length is within the limit
        if (strlen(student.name) > MAX_NAME_LENGTH - 1)
        {
            printf("Name exceeds maximum length. Please enter a shorter name.\n");
            continue;
        }
        break;
    }
    // Input validation loop for date of birth
    while (1)
    {
        printf("Enter date of birth (YYYY-MM-DD): ");
        fgets(student.dateOfBirth, DATE_LENGTH, stdin);
        // Check if the date of birth is not empty
        if (strlen(student.dateOfBirth) == 1 && student.dateOfBirth[0] == '\n')
        {
            printf("Date of birth cannot be empty.\n");
            continue;
        }
        // Remove the newline character if present
        if (student.dateOfBirth[strlen(student.dateOfBirth) - 1] == '\n')
        {
            student.dateOfBirth[strlen(student.dateOfBirth) - 1] = '\0';
        }
        // Check if the date of birth length is valid
        if (strlen(student.dateOfBirth) != 10)
        {
            printf("Invalid date format. Please enter date in YYYY-MM-DD format.\n");
            continue;
        }
        // Additional validation for date format could be implemented here
        break;
    }
    // Input validation loop for registration number
    while (1)
    {
        printf("Enter registration number (6-digit numerical): ");
        fgets(student.registrationNumber, REGISTRATION_LENGTH, stdin);
        // Check if the registration number is not empty
        if (strlen(student.registrationNumber) == 1 && student.registrationNumber[0] == '\n')
        {
            printf("Registration number cannot be empty.\n");
            continue;
        }
        // Remove the newline character if present
        if (student.registrationNumber[strlen(student.registrationNumber) - 1] == '\n')
        {
            student.registrationNumber[strlen(student.registrationNumber) - 1] = '\0';
        }
        // Check if the registration number length is valid
        if (strlen(student.registrationNumber) != 6)
        {
            printf("Invalid registration number length. Please enter a 6-digit numerical value.\n");
            continue;
        }
        // Additional validation for numerical characters could be implemented here
        break;
    }
    // Input validation loop for program code
    while (1)
    {
        printf("Enter program code (up to 4 characters): ");
        fgets(student.programCode, PROGRAM_LENGTH, stdin);
        // Check if the program code is not empty
        if (strlen(student.programCode) == 1 && student.programCode[0] == '\n')
        {
            printf("Program code cannot be empty.\n");
            continue;
        }
        // Remove the newline character if present
        if (student.programCode[strlen(student.programCode) - 1] == '\n')
        {
            student.programCode[strlen(student.programCode) - 1] = '\0';
        }
        // Check if the program code length is valid
        if (strlen(student.programCode) > PROGRAM_LENGTH - 1)
        {
            printf("Program code exceeds maximum length. Please enter a shorter code.\n");
            continue;
        }
        break;
    }
    // Input validation loop for annual tuition
    while (1)
    {
        printf("Enter annual tuition (greater than zero): ");
        scanf("%f", &student.annualTuition);
        getchar(); // Consume the newline character left in the buffer
        // Check if the annual tuition is greater than zero
        if (student.annualTuition <= 0)
        {
            printf("Annual tuition must be greater than zero.\n");
            continue;
        }
        break;
    }
    return student;
}

// Function to create a new node
Node *createNode(Student data)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

// Function to initialize the hash table
void initHashTable(HashTable *hashTable)
{
    hashTable->head = (Node *)malloc(HASH_TABLE_SIZE * sizeof(Node));
    if (hashTable->head == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    for (int i = 0; i < HASH_TABLE_SIZE; i++)
    {
        hashTable->head[i].next = NULL;
    }
}

// Function to calculate hash value for a given registration number
int hash(char *registrationNumber)
{
    int sum = 0;
    for (int i = 0; i < strlen(registrationNumber); i++)
    {
        sum += registrationNumber[i];
    }
    return sum % HASH_TABLE_SIZE;
}

// Function to insert a student into the hash table
void insertStudent(HashTable *hashTable, Student student)
{
    int index = hash(student.registrationNumber);
    Node *newNode = createNode(student);
    newNode->next = hashTable->head[index].next;
    hashTable->head[index].next = newNode;
    printf("Student inserted successfully.\n");
}

// Function to display all students in the hash table
void displayStudents(HashTable *hashTable)
{
    printf("List of Students:\n\n");
    for (int i = 0; i < HASH_TABLE_SIZE; i++)
    {
        Node *current = hashTable->head[i].next;
        while (current != NULL)
        {
            printf("Name: %s \n", current->data.name);
            printf("Date of Birth: %s \n", current->data.dateOfBirth);
            printf("Registration Number: %s \n", current->data.registrationNumber);
            printf("Program Code: %s \n", current->data.programCode);
            printf("Annual Tuition: $%.2f\n \n", current->data.annualTuition);
            current = current->next;
        }
    }
}

// Function to delete a student from the hash table
void deleteStudent(HashTable *hashTable, char *registrationNumber)
{
    int index = hash(registrationNumber);
    Node *current = &hashTable->head[index];
    while (current->next != NULL)
    {
        if (strcmp(current->next->data.registrationNumber, registrationNumber) == 0)
        {
            Node *temp = current->next;
            current->next = current->next->next;
            free(temp);
            printf("Student deleted successfully.\n");
            return;
        }
        current = current->next;
    }
    printf("Student not found.\n");
}

// Function to search for a student by registration number
void searchStudent(HashTable *hashTable, char *registrationNumber)
{
    // Remove the newline character from the registration number
    registrationNumber[strcspn(registrationNumber, "\n")] = '\0';

    int index = hash(registrationNumber);
    Node *current = hashTable->head[index].next;
    while (current != NULL)
    {
        if (strcmp(current->data.registrationNumber, registrationNumber) == 0)
        {
            printf("Student found:\n");
            printf("Name: %s\n", current->data.name);
            printf("Date of Birth: %s\n", current->data.dateOfBirth);
            printf("Registration Number: %s\n", current->data.registrationNumber);
            printf("Program Code: %s\n", current->data.programCode);
            printf("Annual Tuition: $%.2f\n", current->data.annualTuition);
            return;
        }
        current = current->next;
    }
    printf("Student not found.\n");
}

// Function to swap two student records
void swapStudents(Student *a, Student *b)
{
    Student temp = *a;
    *a = *b;
    *b = temp;
}

// Function to sort students based on program code and name
void sortStudents(HashTable *hashTable)
{
    // Bubble sort algorithm
    for (int i = 0; i < HASH_TABLE_SIZE; i++)
    {
        Node *current = hashTable->head[i].next;
        while (current != NULL)
        {
            Node *nextNode = current->next;
            while (nextNode != NULL)
            {
                // Compare program codes
                if (strcmp(current->data.programCode, nextNode->data.programCode) > 0)
                {
                    swapStudents(&current->data, &nextNode->data);
                }
                // If program codes are equal, compare names
                else if (strcmp(current->data.programCode, nextNode->data.programCode) == 0 &&
                         strcmp(current->data.name, nextNode->data.name) > 0)
                {
                    swapStudents(&current->data, &nextNode->data);
                }
                nextNode = nextNode->next;
            }
            current = current->next;
        }
    }
}

// Function to export records to a CSV file
void exportRecords(HashTable *hashTable)
{
    FILE *fp;
    fp = fopen("student_records.csv", "a"); // Open the file in append mode

    if (fp == NULL)
    {
        printf("Error opening file for writing.\n");
        return;
    }

    // Iterate through the hash table and write each student's data to the file
    for (int i = 0; i < HASH_TABLE_SIZE; i++)
    {
        Node *current = hashTable->head[i].next;
        while (current != NULL)
        {
            fprintf(fp, "%s;%s;%s;%s;%.2f\n", current->data.name, current->data.dateOfBirth, current->data.registrationNumber, current->data.programCode, current->data.annualTuition);
            current = current->next;
        }
    }

    fclose(fp); // Close the file
    printf("Records exported successfully to student_records.csv.\n");
}

int main()
{
    HashTable hashTable;
    initHashTable(&hashTable);

    int choice;
    char registrationNumber[REGISTRATION_LENGTH];
    Student newStudent;

    while (1)
    {
        printf("\nMenu:\n");
        printf("1. Add a student\n");
        printf("2. Display all students\n");
        printf("3. Search for a student by registration number\n");
        printf("4. Sort students by program code and name\n");
        printf("5. Export records to CSV file\n");
        printf("6. Delete a student\n");
        printf("7. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // Consume the newline character left in the buffer

        switch (choice)
        {
        case 1:
            newStudent = createStudent();
            insertStudent(&hashTable, newStudent);
            break;
        case 2:
            displayStudents(&hashTable);
            break;
        case 3:
            printf("Enter registration number of student to search: ");
            fgets(registrationNumber, REGISTRATION_LENGTH, stdin);
            searchStudent(&hashTable, registrationNumber);
            break;
        case 4:
            sortStudents(&hashTable);
            printf("Students sorted successfully.\n");
            break;
        case 5:
            exportRecords(&hashTable);
            break;
        case 6:
            printf("Enter registration number of student to delete: ");
            fgets(registrationNumber, REGISTRATION_LENGTH, stdin);
            deleteStudent(&hashTable, registrationNumber);
            break;
        case 7:
            printf("Exiting...\n");
            exit(0);
        default:
            printf("Invalid choice. Please enter a number from 1 to 7.\n");
        }
    }

    return 0;
}
